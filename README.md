# Kubernetes 

## Contexte
Le TP a été réalisé avec :
- Une VM Centos-7 comme client
- 1 VM Centos-7 pour le master et 2 VMs Centos-7 worker

## Partie 1
1. Créer un daemonSet nommé producteur basé sur l'image alpine
2. Créer un volume nommé web
3. Monter le volume web sur le conteneur
4. Ecrire dans le fichier /web/index.html le nom du hostname et la date toutes les 60 secondes

## Partie 2
1. Créer un deploiement nommé web (avec 2 replicas) basé sur l'image httpd
2. Monter le volume web créé précédemment sur le chemin htdocs du serveur Apache

## Partie 3
1. Créer un service nommé web de type NodePort qui rassemble les pods du deploiement web
